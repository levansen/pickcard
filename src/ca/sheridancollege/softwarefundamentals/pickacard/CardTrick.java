/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.lang.Math;
import java.util.Scanner;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        Card[] magicHand = new Card[7];
        
        
        
        Scanner in = new Scanner(System.in);
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c= new Card();
            int random_Card_Num = (int)(Math.random() * 13 ) + 1;
           c.setValue(random_Card_Num);
           
           int random_Suite_Num = (int)(Math.random() * 4);
            c.setSuit(Card.SUITS[random_Suite_Num]);
            magicHand[i] = c;
        }
                
   
      System.out.println(magicHand[3]);
        
        System.out.println("Enter Card Value: ");
        int cardVal = in.nextInt();
        System.out.println("Enter Suite: ");
        String suiteVal = in.next();
        
        Card userCard = new Card();
        userCard.setValue(cardVal);
        userCard.setSuit(suiteVal);
        //pokemon
        boolean doesExist = false;
        for(int i=0; i<magicHand.length; i++){
           if ((magicHand[i].getValue() == userCard.getValue()) && magicHand[i].getSuit().equals(userCard.getSuit())) {
                System.out.println("Card is found in magic hand");
                doesExist = true;
                break;
            }
            else if(doesExist == false)
                System.out.println("Card is not found in the magic hand");
        }
    }
    
}
